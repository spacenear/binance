import React, {Component} from 'react';
import Market             from './Components/Market/Market';
import './App.scss';

class App extends Component {
  render() {
    return (
      <div className={'app'}>
        <Market/>
      </div>
    );
  }
}

export default App;
