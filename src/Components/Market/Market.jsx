import React, {Component} from 'react';
import './Market.scss';
import marketData         from './../../api';
import Scrollbar          from 'react-scrollbars-custom';

class Market extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      activeNavButton: 'BTC',
      market:          marketData.data,
      favorites:       [],
      valueStatus:     'change',
      sorting:         ['pair', 'asc'],
      search:          '',
    }
    
    this.navButtons = [
      {'favorite': (<i className="fas fa-star"></i>)},
      {'BTC': 'BTC'},
      {'BNB': 'BNB'},
      {'ALTS': 'ALTS'},
      {'USDⓈ': 'USD'},
    ];
  }
  
  componentDidMount() {
    this.socketConnect();
  }
  
  socketConnect() {
    let socket = new WebSocket("wss://stream.binance.com/stream?streams=!miniTicker@arr");
    
    socket.onopen = () => {
      console.log('Connection established.');
    };
    
    socket.onclose = event => {
      this.socketConnect();
      
      if (event.wasClean) {
        console.log('Connection closed cleanly');
      } else {
        console.log('Connection lost');
      }
      
      console.log('Code: ' + event.code + ' reason: ' + event.reason);
    };
    
    socket.onmessage = event => {
      this.updateMarket(JSON.parse(event.data).data);
    };
    
    socket.onerror = error => {
      console.log('Error ' + error.message);
    };
  }
  
  updateMarket(data) {
    let market = this.state.market;
    
    for (let entity of data) {
      for (let marketEntity of market) {
        if (marketEntity.s === entity.s) {
          marketEntity.c = entity.c;
          marketEntity.o = entity.o;
        }
      }
    }
    
    this.setState({market});
  }
  
  filterMarket() {
    let filteredMarket = [];
    
    this.state.market.map(elem => {
      if (
        this.state.activeNavButton === elem.q || this.state.activeNavButton === elem.pm ||
        (this.state.favorites.includes(elem.s) && this.state.activeNavButton === 'favorite')
      ) {
        if (this.state.search !== '') {
          let name = elem.b + '/' + elem.q;
          
          if (name.includes(this.state.search)) {
            filteredMarket.push(elem);
          }
        } else {
          filteredMarket.push(elem);
        }
      }
    });
    
    return filteredMarket;
  }
  
  sortMarket(market) {
    if (this.state.sorting[0] === 'pair' && this.state.sorting[1] === 'desc') {
      function usort(a, b) {
        if (a.b < b.b) {
          return 1;
        } else if (a.b > b.b) {
          return -1;
        } else {
          return 0;
        }
      }
      
      return market.sort(usort);
    }
    
    if (this.state.sorting[0] === 'pair' && this.state.sorting[1] === 'asc') {
      function usort(a, b) {
        if (a.b > b.b) {
          return 1;
        } else if (a.b < b.b) {
          return -1;
        } else {
          return 0;
        }
      }
      
      return market.sort(usort);
    }
    
    if (this.state.sorting[0] === 'last-price' && this.state.sorting[1] === 'desc') {
      function usort(a, b) {
        if (a.c < b.c) {
          return 1;
        } else if (a.c > b.c) {
          return -1;
        } else {
          return 0;
        }
      }
      
      return market.sort(usort);
    }
    
    if (this.state.sorting[0] === 'last-price' && this.state.sorting[1] === 'asc') {
      function usort(a, b) {
        if (a.c > b.c) {
          return 1;
        } else if (a.c < b.c) {
          return -1;
        } else {
          return 0;
        }
      }
      
      return market.sort(usort);
    }
    
    if (this.state.sorting[0] === 'change' && this.state.sorting[1] === 'desc') {
      function usort(a, b) {
        if (this.state.valueStatus === 'change') {
          if (((a.o - a.c) / (a.o / 100)) < (b.o - b.c) / (b.o / 100)) {
            return 1;
          } else if (((a.o - a.c) / (a.o / 100)) > (b.o - b.c) / (b.o / 100)) {
            return -1;
          } else {
            return 0;
          }
        } else {
          if (a.v < b.v) {
            return 1;
          } else if (a.v > b.v) {
            return -1;
          } else {
            return 0;
          }
        }
      }
      
      return market.sort(usort.bind(this));
    }
    
    if (this.state.sorting[0] === 'change' && this.state.sorting[1] === 'asc') {
      function usort(a, b) {
        if (this.state.valueStatus === 'change') {
          if (((a.o - a.c) / (a.o / 100)) > (b.o - b.c) / (b.o / 100)) {
            return 1;
          } else if (((a.o - a.c) / (a.o / 100)) < (b.o - b.c) / (b.o / 100)) {
            return -1;
          } else {
            return 0;
          }
        } else {
          if (a.v > b.v) {
            return 1;
          } else if (a.v < b.v) {
            return -1;
          } else {
            return 0;
          }
        }
      }
      
      return market.sort(usort.bind(this));
    }
    
    return market;
  }
  
  setNavButtonClass(buttonAlias) {
    return this.state.activeNavButton === buttonAlias ? ' active' : ''
  }
  
  navButtonClickHandler(buttonAlias) {
    this.setState({activeNavButton: buttonAlias});
  }
  
  addToFavoriteButtonClickHandler(pairName) {
    let favorites = this.state.favorites;
    
    if (favorites.includes(pairName)) {
      favorites.splice(favorites.indexOf(pairName), 1);
    } else {
      favorites.push(pairName);
    }
    
    this.setState({favorites});
  }
  
  sortButtonclickHandler(sortingName) {
    let sorting = this.state.sorting[1];
    
    if (this.state.sorting[0] === sortingName) {
      sorting = sorting === 'asc' ? 'desc' : 'asc';
    } else {
      sorting = 'asc';
    }
    
    this.setState({sorting: [sortingName, sorting]});
  }
  
  radioButtonClickHandler(value) {
    this.setState({valueStatus: value});
  }
  
  searchInputChangeHandler(e) {
    this.setState({search: e.target.value.toUpperCase()});
  }
  
  render() {
    let navButtons = this.navButtons.map((elem, index) => {
      return (
        <div className={'navline-button' + this.setNavButtonClass(Object.keys(elem)[0])}
             onClick={e => this.navButtonClickHandler(Object.keys(elem)[0])}
             key={index}
             data-pm={Object.keys(elem)[0]}
        >
          {elem[Object.keys(elem)[0]]}
        </div>
      );
    });
    
    let market = this.sortMarket(this.filterMarket()).map((elem, index) => {
      let change, changeClass;
      let favoriteClasses = ['add-to-favotite-button'];
      
      if (this.state.favorites.includes(elem.s)) {
        favoriteClasses.push('active');
      }
      
      if (this.state.valueStatus === 'change') {
        change      = ((elem.o - elem.c) / (elem.o / 100)).toFixed(2);
        changeClass = change >= 0 ? 'success' : 'error';
        change += '%';
      } else {
        change = elem.v.toFixed(0);
      }
      
      return (
        <tr key={index}>
          <td data-pm={elem.pm}>
            <div className={favoriteClasses.join(' ')}
                 onClick={e => this.addToFavoriteButtonClickHandler(elem.s)}
            >
              <i className="fas fa-star"></i>
            </div>
            {elem.b + '/' + elem.q}
          </td>
          <td>
            {Number(elem.c).toFixed(10).replace(/\.?0+$/, '')}
          </td>
          <td className={changeClass}>
            {change}
          </td>
        </tr>
      );
    });
    
    let content = (
      <div className={'empty-tab'}>
        Nothing here yet
      </div>
    );
    
    if (market.length > 0) {
      content = (
        <Scrollbar>
          <table>
            <thead>
            <tr>
              <td>
                <div className={'sort-button'}
                     onClick={e => this.sortButtonclickHandler('pair')}
                >
                  Pair
                  <div className={'sort-button-indicators'}>
                    <i className={
                      'fas fa-sort-up' +
                      (
                        this.state.sorting[0] === 'pair' &&
                        this.state.sorting[1] === 'desc' ? ' active' : ''
                      )
                    }></i>
                    <i className={
                      'fas fa-sort-down' +
                      (
                        this.state.sorting[0] === 'pair' &&
                        this.state.sorting[1] === 'asc' ? ' active' : ''
                      )
                    }></i>
                  </div>
                </div>
              </td>
              <td>
                <div className={'sort-button'}
                     onClick={e => this.sortButtonclickHandler('last-price')}
                >
                  Last Price
                  <div className={'sort-button-indicators'}>
                    <i className={
                      'fas fa-sort-up' +
                      (
                        this.state.sorting[0] === 'last-price' &&
                        this.state.sorting[1] === 'desc' ? ' active' : ''
                      )
                    }></i>
                    <i className={
                      'fas fa-sort-down' +
                      (
                        this.state.sorting[0] === 'last-price' &&
                        this.state.sorting[1] === 'asc' ? ' active' : ''
                      )
                    }></i>
                  </div>
                </div>
              </td>
              <td>
                <div className={'sort-button'}
                     onClick={e => this.sortButtonclickHandler('change')}
                >
                  {this.state.valueStatus === 'change' ? 'Change' : 'Volume'}
                  <div className={'sort-button-indicators'}>
                    <i className={
                      'fas fa-sort-up' +
                      (
                        this.state.sorting[0] === 'change' &&
                        this.state.sorting[1] === 'desc' ? ' active' : ''
                      )
                    }></i>
                    <i className={
                      'fas fa-sort-down' +
                      (
                        this.state.sorting[0] === 'change' &&
                        this.state.sorting[1] === 'asc' ? ' active' : ''
                      )
                    }></i>
                  </div>
                </div>
              </td>
            </tr>
            </thead>
            <tbody>
            {market}
            </tbody>
          </table>
        </Scrollbar>
      );
    }
    
    return (
      <div className={'market-wiget'}>
        <div className={'market-wiget-header'}>
          Market
        </div>
        <div className={'market-wiget-navline'}>
          {navButtons}
        </div>
        <div className={'market-wiget-toolbar'}>
          <div className={'search-wrap'}>
            <input type="text"
                   maxLength={20}
                   placeholder={'Search'}
                   onChange={e => this.searchInputChangeHandler(e)}
            />
          </div>
          <div className={'toggle-wrap'}>
            <div className={'radio-button' + (this.state.valueStatus === 'change' ? ' active' : '')}
                 onClick={e => this.radioButtonClickHandler('change')}
            >
              <div className={'radio-button-point'}></div>
              <div className={'radio-button-title'}>
                Change
              </div>
            </div>
            <div className={'radio-button' + (this.state.valueStatus === 'volume' ? ' active' : '')}
                 onClick={e => this.radioButtonClickHandler('volume')}
            >
              <div className={'radio-button-point'}></div>
              <div className={'radio-button-title'}>
                Volume
              </div>
            </div>
          </div>
        </div>
        <div className={'market-wiget-table-wrap'}>
          {content}
        </div>
      </div>
    );
  }
}

export default Market;