describe('Add to favorite', function () {
  before(() => {
    cy.visit('http://localhost:3000/');
  });
  
  const favorites = [];
  
  it('Add pairs to favorite', function () {
    cy.get('tbody').find('tr').find('td:first').each($elem => {
      let rand = Math.floor(1 + Math.random() * (5 + 1 - 1));
      if (rand === 1) {
        $elem.find('.add-to-favotite-button').click();
        favorites.push($elem.text());
      }
    });
  });
  
  it('Open favorite tab', function () {
    cy.get('.navline-button[data-pm="favorite"]').click();
  });
  
  it('Check pairs in favorites', function () {
    let status = true;
    
    cy.get('tbody').find('tr').find('td:first').each($elem => {
      if(!favorites.includes($elem.text())) {
        status = false;
      }
    });
  
    expect(status).to.be.true;
  });
});