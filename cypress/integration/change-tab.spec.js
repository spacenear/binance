describe('Change Tab', function () {
  before(() => {
    cy.visit('http://localhost:3000/');
  });
  
  it('Tab click', function () {
    cy.get('.navline-button').contains('USD').click().then($btn => {
      expect($btn.hasClass('active')).to.be.true;
      
      let status = true;
      
      cy.get('tbody').find('tr').find('td:first').each($elem => {
        if (status) {
          status = $elem.attr('data-pm') === $btn.attr('data-pm');
        }
      });
      
      expect(status).to.be.true;
    });
  });
});