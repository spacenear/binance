I spent about one day on this project.

I did not understand what the `Margin` tab means and did not include it in the project.

Also, in the `ALTS` and `USD` tabs, the properties of `pm` and `pn` are the same everywhere. So, I didn't them as a drop-down.

In the project directory, you can run:

##### `node_modules\.bin\cypress open`

It runs the cypress e2e tests

Tests are stored in `cypress/integration` folder

You can check how my widget works on [that web site](http://widget.eclipseweb.ru/)